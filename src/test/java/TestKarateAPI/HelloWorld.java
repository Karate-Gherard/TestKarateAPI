package TestKarateAPI;

import com.intuit.karate.junit5.Karate;

public class HelloWorld {

	@Karate.Test
	Karate helloWorld() {
		return Karate.run("hello-world").tags("@tag1").relativeTo(getClass());
	}
}
